# Bnojourator

*Tout a commencé avec une faute de frappe, aujourd'hui devenu le souhait de bonne journée par défaut utilisé auprès de ses amis et collègues*

Le Random Generator of "Bnojour" (RGB) ou *Bnojourator* permet de générer un Bnojour avec une fonte de caractères choisie aléatoirement, pour diversifier sa façon de saluer
