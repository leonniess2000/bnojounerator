/**
 * Random style generator
 */

/** Random utilities */
rand = (size) => Math.floor(Math.random() * size)
arrayRand = (arr) => arr[rand(arr.length)]

/** Retrieve font from Google using its name */
googleFontUrl = (name) => `https://fonts.googleapis.com/css2?family=${name}&display=swap`

/** Retrieve multiples fonts from Google using their names */
googleFontsUrl = (names) => {
    const paramsList = names
        .map(name => `family=${name}`)
        .reduce((acc, val) => `${acc}&${val}`)

    return `https://fonts.googleapis.com/css2?${paramsList}&display=swap`
}

const RandomFont = {
    /** Font database */
    fonts: [
        { name: "Abril Fatface", url: googleFontUrl('Abril+Fatface') },
        { name: "Akaya Telivigala", url: googleFontUrl('Akaya+Telivigala') },
        { name: "Arbutus", url: googleFontUrl('Arbutus') },
        { name: "Architects Daughter", url: googleFontUrl('Architects+Daughter') },
        { name: "Bangers", url: googleFontUrl('Bangers') },
        { name: "Barrio", url: googleFontUrl('Barrio') },
        { name: "Bebas Neue", url: googleFontUrl('Bebas+Neue') },
        { name: "Bigelow Rules", url: googleFontUrl('Bigelow+Rules') },
        { name: "Bungee Shade", url: googleFontUrl('Bungee+Shade') },
        { name: "Caesar Dressing", url: googleFontUrl('Caesar+Dressing') },
        { name: "Caveat", url: googleFontUrl('Caveat') },
        { name: "Charm", url: googleFontUrl('Charm') },
        { name: "Cookie", url: googleFontUrl('Cookie') },
        { name: "Codystar", url: googleFontUrl('Codystar') },
        { name: "Creepster", url: googleFontUrl('Creepster') },
        { name: "Dancing Script", url: googleFontUrl('Dancing+Script') },
        { name: "Eater", url: googleFontUrl('Eater') },
        { name: "Ewert", url: googleFontUrl('Ewert') },
        { name: "Faster One", url: googleFontUrl('Faster+One') },
        { name: "Festive", url: googleFontUrl('Festive') },
        { name: "Fredericka the Great", url: googleFontUrl('Fredericka+the+Great') },
        { name: "Gideon Roman", url: googleFontUrl('Gideon+Roman') },
        { name: "Gloria Hallelujah", url: googleFontUrl('Gloria+Hallelujah') },
        { name: "Hanalei", url: googleFontUrl('Hanalei') },
        { name: "Handlee", url: googleFontUrl('Handlee') },
        { name: "Homemade Apple", url: googleFontUrl('Homemade+Apple') },
        { name: "Inspiration", url: googleFontUrl('Inspiration') },
        { name: "Kalam", url: googleFontUrl('Kalam') },
        { name: "Kaushan Script", url: googleFontUrl('Kaushan+Script') },
        { name: "Kolker Brush", url: googleFontUrl('Kolker+Brush') },
        { name: "Kumar One Outline", url: googleFontUrl('Kumar+One+Outline') },
        { name: "Indie Flower", url: googleFontUrl('Indie+Flower') },
        { name: "Lakki Reddy", url: googleFontUrl('Lakki+Reddy') },
        { name: "Libre Barcode 39 Text", url: googleFontUrl('Libre+Barcode+39+Text') },
        { name: "Lobster", url: googleFontUrl('Lobster') },
        { name: "Londrina Outline", url: googleFontUrl('Londrina+Outline') },
        { name: "Luckiest Guy", url: googleFontUrl('Luckiest+Guy') },
        { name: "Major Mono Display", url: googleFontUrl('Major+Mono+Display') },
        { name: "Meddon", url: googleFontUrl('Meddon') },
        { name: "Modak", url: googleFontUrl('Modak') },
        { name: "Monofett", url: googleFontUrl('Monofett') },
        { name: "Monoton", url: googleFontUrl('Monoton') },
        { name: "Moo Lah Lah", url: googleFontUrl('Moo+Lah+Lah') },
        { name: "Mrs Sheppards", url: googleFontUrl('Mrs+Sheppards') },
        { name: "Neonderthaw", url: googleFontUrl('Neonderthaw') },
        { name: "Nosifer", url: googleFontUrl('Nosifer') },
        { name: "Ole", url: googleFontUrl('Ole') },
        { name: "Orbitron", url: googleFontUrl('Orbitron') },
        { name: "Oswald", url: googleFontUrl('Oswald') },
        { name: "Pacifico", url: googleFontUrl('Pacifico') },
        { name: "Parisienne", url: googleFontUrl('Parisienne') },
        { name: "Paytone One", url: googleFontUrl('Paytone+One') },
        { name: "Permanent Marker", url: googleFontUrl('Permanent+Marker') },
        { name: "Pirata One", url: googleFontUrl('Pirata+One') },
        { name: "Plaster", url: googleFontUrl('Plaster') },
        { name: "Playball", url: googleFontUrl('Playball') },
        { name: "Press Start 2P", url: googleFontUrl('Press+Start+2P') },
        { name: "Rajdhani", url: googleFontUrl('Rajdhani') },
        { name: "Rampart One", url: googleFontUrl('Rampart+One') },
        { name: "Redressed", url: googleFontUrl('Redressed') },
        { name: "Roboto", url: googleFontUrl('Roboto') },
        { name: "Rowdies", url: googleFontUrl('Rowdies') },
        { name: "Rock Salt", url: googleFontUrl('Rock+Salt') },
        { name: "Rubik Beastly", url: googleFontUrl('Rubik+Beastly') },
        { name: "Sacramento", url: googleFontUrl('Sacramento') },
        { name: "Satisfy", url: googleFontUrl('Satisfy') },
        { name: "Shadows Into Light", url: googleFontUrl('Shadows+Into+Light') },
        { name: "Shizuru", url: googleFontUrl('Shizuru') },
        { name: "Shojumaru", url: googleFontUrl('Shojumaru') },
        { name: "Syne Tactile", url: googleFontUrl('Syne+Tactile') },
        { name: "Tangerine", url: googleFontUrl('Tangerine') },
        { name: "Tourney", url: googleFontUrl('Tourney') },
        { name: "Train One", url: googleFontUrl('Train+One') },
        { name: "Vast Shadow", url: googleFontUrl('Vast+Shadow') },
        { name: "Wallpoet", url: googleFontUrl('Wallpoet') },
        { name: "Yomogi", url: googleFontUrl('Yomogi') },
        { name: "Yuji Hentaigana Akari", url: googleFontUrl('Yuji+Hentaigana+Akari') },
        { name: "Zen Tokyo Zoo", url: googleFontUrl('Zen+Tokyo+Zoo') },
        { name: "Zilla Slab Highlight", url: googleFontUrl('Zilla+Slab+Highlight') }
    ],

    /** Keep track of fonts that were already loaded */
    loadedFonts: new Set(),

    /**
     * Apply a random font on a DOM Element
     * @param {Element} domElt DOM Element to which random font will be applied
     */
    applyRandomFont: (domElt) => {
        const applyFont = (fontName) => {
            domElt.style.setProperty('font-family', `'${fontName}'`)
        }

        if(!RandomFont._init)
        {
            RandomFont._init = true
            RandomFont.forceLoadAll()
        }

        const randFont = arrayRand(RandomFont.fonts)
        if(!RandomFont.loadedFonts.has(randFont.name))
        {
            const stylesheetElt = document.createElement('link')
            stylesheetElt.type = 'text/css'
            stylesheetElt.rel = 'stylesheet'
            stylesheetElt.crossOrigin = "anonymous" // Resolve CORS issues
            stylesheetElt.href = randFont.url

            stylesheetElt.onload = () => {
                document.fonts.ready.then(() => {
                    RandomFont.loadedFonts.add(randFont.name)
                    applyFont(randFont.name)
                })
            }

            document.head.appendChild(stylesheetElt)
        }
        else
            applyFont(randFont.name)
    },

    /** List of fonts not already loaded */
    notLoadedFonts: () => RandomFont.fonts.filter(font => !RandomFont.loadedFonts.has(font.name)),

    /** DEBUG Only: force load of all fonts not already loaded */
    forceLoadAll: () => {
        const fonts = RandomFont.notLoadedFonts();
        if(fonts.length <= 0)
        {
            console.log("All fonts already loaded")
            return
        }

        const fontsNames = fonts.map(font => font.name);
        const fontsUrl = googleFontsUrl(fontsNames);

        console.log(`Fonts to be loaded: ${fontsNames.reduce((a, b) => `${a}, ${b}`)}`)

        const stylesheetElt = document.createElement('link')
        stylesheetElt.type = 'text/css'
        stylesheetElt.rel = 'stylesheet'
        stylesheetElt.crossOrigin = "anonymous" // Resolve CORS issues
        stylesheetElt.href = fontsUrl

        stylesheetElt.onload = () => {
            console.log("Stylesheet successfully loaded")

            document.fonts.ready.then(() => {
                fontsNames.forEach(font => RandomFont.loadedFonts.add(font))

                console.log("Fonts loaded. Checking ...")
                fontsNames.forEach(font => {
                    if(document.fonts.check(`12px '${font}'`, 'bnojour'))
                        console.error(`Font ${font} is not available`)
                })
            })
        }

        document.head.appendChild(stylesheetElt)
    },

    _init: false
}

const COLOR_COMPONENT_MAX = 256

const RandomColor = {
    /**
     * Generate a random css color string in the form of 'rgb(red, green, blue)'
     * @param {Number} minLight Minimum luminosity of the color
     * @param {Number} maxLight Maximum luminosity of the color
     */
    randomColorString: (minLight, maxLight) => {
        const lightBase = Math.min(minLight, COLOR_COMPONENT_MAX - 1)
        const lightUpper = Math.min(maxLight, COLOR_COMPONENT_MAX - 1)

        const randComponent = () => lightBase + rand(lightUpper - lightBase)

        const r = randComponent(), g = randComponent(), b = randComponent()
        return `rgb(${r}, ${g}, ${b})`
    },

    /** Minimum lightness of random colors */
    lightnessMin: 69,

    /** Maximum lightness of random colors */
    lightnessMax: 242,

    /**
     * Apply random color to given Dom Element
     */
    applyRandomColor: (domElt) => {
        domElt.style.setProperty('color',
            RandomColor.randomColorString(RandomColor.lightnessMin,
                                            RandomColor.lightnessMax))
    }
}
